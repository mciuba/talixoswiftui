import Combine
import Foundation
import SwiftUI

public class VehiclesViewModel: BindableObject {
    public typealias PublisherType = PassthroughSubject<VehiclesViewModel, Never>
    public let didChange = PublisherType()

    public var vehicles = Vehicle.sample
    public var searchString: String = "" {
        didSet {
            vehicles = Vehicle.sample.filtered(by: searchString)
            didChange.send(self)
        }
    }
    
    public init() { }
    
    // TODO workaround 
    public static var singleton = VehiclesViewModel()
}

public class Vehicle: BindableObject {
    public typealias PublisherType = PassthroughSubject<Vehicle, Never>
    public let didChange = PublisherType()
    
    public let name: String
    public let plateNumber: String
    public var availability: Availability {
        didSet {
            didChange.send(self)
        }
    }
    
    public init(name: String, plateNumber: String, availability: Availability) {
        self.name = name
        self.plateNumber = plateNumber
        self.availability = .unknown
    }
    
    public static var sample: [Vehicle] = {
        let manufacturers: [String] = [
            "Mercedes",
            "BMW",
            "Audi",
            "Volkswagen",
            "Toyota",
            "Bentley",
            "Porsche"
        ]
        
        let models = (0...30).map { (Int) -> Vehicle in
            let manufacturer = manufacturers.randomElement()!
            let someNo = randomInt()
            let plateNumber = "TLX \(someNo)"
            return Vehicle(name: manufacturer, plateNumber: plateNumber, availability: .unknown)
        }
        return models
    }()
}


public extension Vehicle {
    func fetchAvailability() {
//        print("Fetching availability for \(plateNumber)")
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(1000)) {
            self.availability = .fakeResolved()
//            print("Fetched availability \(self.availability) for \(self.plateNumber)")
        }
    }
}

public enum Availability: Int {
    case unknown
    case available
    case inUse
}

public extension Availability {
    var description: String {
        switch self {
        case .unknown: return "Unknown"
        case .available: return "Available"
        case .inUse: return "In use"
        }
    }
    
    static func fakeResolved() -> Availability {
        return randomInt() % 2 > 0 ? .available : .inUse
    }
}

func randomInt() -> Int {
    return Int.random(in: 1000...9999)
}

extension Array where Element: Vehicle {
    func filtered(by searchString: String) -> Self {
        print("Searching by: \(searchString)")
        guard !searchString.isEmpty else {
            return self
        }
        
        return self.filter {
            $0.name.lowercased().localizedCaseInsensitiveContains(searchString) ||
            $0.plateNumber.lowercased().localizedCaseInsensitiveContains(searchString)
        }
    }
}
