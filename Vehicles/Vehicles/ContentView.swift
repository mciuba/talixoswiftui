//
//  ContentView.swift
//  Vehicles
//
//  Created by Michal Ciuba on 07/06/2019.
//  Copyright © 2019 talixo. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @ObjectBinding var viewModel: VehiclesViewModel = VehiclesViewModel()
    var body: some View {
        VStack(alignment: .leading) {
            TextField($viewModel.searchString, placeholder: .init(verbatim: "Search"))
                .padding(16)
            List(viewModel.vehicles.identified(by: \.plateNumber)) { vehicle in
                ListCell(model: vehicle)
            }
        }
        .padding([.top], 16)
    }
}

struct ListCell: View {
    @State var model: Vehicle
    
    var body: some View {
        VStack(alignment: .leading) {
            VStack(alignment: .leading) {
                Text(model.name).bold()
                Text(model.plateNumber)
            }
            HStack(alignment: .center) {
                Spacer(minLength: 16)
                AvailabilityView(model: model)
            }
            }
            .padding(8)
    }
    
    
}

struct AvailabilityView: View {
    @State var model: Vehicle
    
    var body: some View {
        Text(model.availability.description)
            //                .animation(nil)
            .color(determineColor())
            .animation(.fluidSpring())
            .onAppear(perform: model.fetchAvailability)
    }
    
    func determineColor() -> Color {
        switch model.availability {
        case .unknown: return .black
        case .available: return .green
        case .inUse: return .orange
        }
    }
}

#if DEBUG
//struct ContentView_Previews : PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}
#endif
